package ar.utn.ppai.disenio.controlador;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import ar.utn.ppai.disenio.modelo.DetalleExposicion;
import ar.utn.ppai.disenio.modelo.Empleado;
import ar.utn.ppai.disenio.modelo.Escuela;
import ar.utn.ppai.disenio.modelo.Exposicion;
import ar.utn.ppai.disenio.modelo.Obra;
import ar.utn.ppai.disenio.modelo.PublicoDestino;
import ar.utn.ppai.disenio.modelo.Sede;
import ar.utn.ppai.disenio.modelo.TipoExposicion;
import ar.utn.ppai.disenio.modelo.TipoVisita;
import ar.utn.ppai.disenio.modelo.infra.PersistenceManager;
import ar.utn.ppai.disenio.modelo.infra.repositories.EscuelaRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.ExposicionRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.SedeRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.TipoVisitaRepository;

@SuppressWarnings("unchecked")
public class ControlRegistrarVisitas {
	
	private final EscuelaRepository escuelaRepository;
	private final SedeRepository sedeRepository;
	private final TipoVisitaRepository tipoVisitaRepository; 
	private static final EntityManager em = PersistenceManager.getInstance().entityManager();
	
	
	public ControlRegistrarVisitas(EscuelaRepository escuelaRepository, SedeRepository sedeRepository,
								   TipoVisitaRepository tipoVisitaRepository) {
		
		this.escuelaRepository = escuelaRepository;
		this.sedeRepository = sedeRepository;
		this.tipoVisitaRepository = tipoVisitaRepository;
	}

	public void buscarEmpleadoLogueado() {
		
	}

	public List<Escuela> buscarEscuela() {
		
		return escuelaRepository.findAll();
//		PersistenceManager.close();
	}

	public void buscarEstadoReserva() {
	}

	public List<Exposicion> buscarExpSedeSeleccionada(Sede sede) {
		System.out.println("------------------- INICIO buscarExpSedeSeleccionada --------------------");
		
		String temporal = "Temporal";
		Date actual = tomarFechaHoraActual();
		
		List<Exposicion> exposiciones = sede.exposicionSedeSeleccionada(actual, temporal);
		for (Exposicion exposicion : exposiciones) {
			System.out.println("buscarExpSedeSeleccionada" + exposicion);
		}
		System.out.println("------------------- FIN buscarExpSedeSeleccionada --------------------");
		
		return exposiciones;
	}

	public void buscarGuiasDispFechaReserva(Sede sede) {
	
		Empleado empleado = new Empleado();
		List<String> guiasDisponibles =  empleado.getGuiaDispEnHorario(sede);
		
		for (String guias : guiasDisponibles) {
			System.out.println("Guias: " + guias);
		}	
	}

	public List<Sede> buscarSede() {
		
		return sedeRepository.findAll();
		
	}

	public List<TipoVisita> buscarTipoVisita() {
		
		return tipoVisitaRepository.findAll();
	
	}

	public void buscarUltNumeroReserva() {
	}

	public void calcCantidadGuiasNecesarios() {
	}

	public boolean calcularCapacidadMaxima(int cantidadavalidar) {
		
		sede.getCantidadMaximaVisitante();
		sede.buscarReservaParaHoraYFecha(sede);
		
		// buscar listas de reservas where con fecha, hora y sede seleccioonada
		cantidadmax = 0;
		for listaReservasObtenidas{
			cantidadmax += itemreserva.cantidadVisitantes
		}
		
		if(cantidadmax<=cantidadavalidar) {
			return true;
		} else {
			return false;
		}
	}

	public void calcularDuracionReserva(Sede sede, List<Exposicion> exposicionesSelecc) {
		System.out.println("------------------- INICIO calcularDuracionReserva --------------------");
		
		String tipoVisitaExposicion = "Por Exposición";
		String tipoVisitaTemporal = "Completa";
		
		int duracionTotalReserva = sede.obtenerDuracionReserva(exposicionesSelecc, tipoVisitaTemporal);
		
		System.out.println("Sede: " + sede.getNombre());
		System.out.println("Duracion Total de la reserva: " + duracionTotalReserva);
		
		System.out.println("------------------- FIN calcularDuracionReserva --------------------");
	}

	public void mostrarEscuelas() {
	}

	public void registrarReserva() {
	}

	public void regReservaVisitaGuiada() {
	}

	public void tomarCantVisitantes() {
	}

	public void tomarEscuela() {
	}

	public void tomarExposicionAVisitar() {
	}

	public Date tomarFechaHoraActual() {
		
		Date fechaActual = new Date();
		
		return fechaActual;
	}

	public void tomarFechaHoraReserva() {
	}

	public void tomarGuia() {
	}

	public void tomarSedeAVisitar() {
	}

	public void tomarTipoVisitaExposicion() {
	}
}



