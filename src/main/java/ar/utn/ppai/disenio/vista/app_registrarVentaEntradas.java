package ar.utn.ppai.disenio.vista;

import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import ar.utn.ppai.disenio.controlador.ControlRegistrarVisitas;
import ar.utn.ppai.disenio.modelo.DetalleExposicion;
import ar.utn.ppai.disenio.modelo.Escuela;
import ar.utn.ppai.disenio.modelo.Exposicion;
import ar.utn.ppai.disenio.modelo.Obra;
import ar.utn.ppai.disenio.modelo.PublicoDestino;
import ar.utn.ppai.disenio.modelo.Sede;
import ar.utn.ppai.disenio.modelo.TipoExposicion;
import ar.utn.ppai.disenio.modelo.TipoVisita;
import ar.utn.ppai.disenio.modelo.infra.PersistenceManager;
import ar.utn.ppai.disenio.modelo.infra.repositories.EscuelaRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.ExposicionRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.SedeRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.TipoVisitaRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.impl.EscuelaRepositoryJpa;
import ar.utn.ppai.disenio.modelo.infra.repositories.impl.ExposicionRepositoryJpa;
import ar.utn.ppai.disenio.modelo.infra.repositories.impl.SedeRepositoryJpa;
import ar.utn.ppai.disenio.modelo.infra.repositories.impl.TipoVisitaRepositoryJpa;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JComponent;

//import com.toedter.calendar.JCalendar;
//import com.toedter.calendar.JDayChooser;
//import com.toedter.calendar.JDateChooser;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JTable;
import java.awt.Dimension;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.JScrollPane;
import javax.persistence.EntityManager;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class app_registrarVentaEntradas extends JFrame {

	private static final EntityManager em = PersistenceManager.getInstance().entityManager();
	private static final ExposicionRepository EXPOSICION_REPOSITORY = new ExposicionRepositoryJpa(PersistenceManager.getInstance().entityManager());
	private static final EscuelaRepository ESCUELA_REPOSITORY= new EscuelaRepositoryJpa(PersistenceManager.getInstance().entityManager());
	private static final SedeRepository SEDE_REPOSITORY = new SedeRepositoryJpa(PersistenceManager.getInstance().entityManager());
	private static final TipoVisitaRepository TIPO_VISITA_REPOSITORY = new TipoVisitaRepositoryJpa(PersistenceManager.getInstance().entityManager());
	private static final ControlRegistrarVisitas CONTROL_REGISTRAR_VISITAS = new ControlRegistrarVisitas(ESCUELA_REPOSITORY, SEDE_REPOSITORY, TIPO_VISITA_REPOSITORY);
	
	private JPanel contentPane;
	private JTextField txtCantVisitantes;
	private JTextField txtDuracionVisita;
	private JComboBox comboEscuela;
	private JComboBox comboSedeVisitar;
	private JComboBox comboTipoVisita;
//	private JDateChooser dateSelectFechaReserva;
	private JButton btnRegistrarReserva;
	private JButton btnCancelarReserva;
	private JLabel lblEscuela;
	private JLabel lblCantidadVisitantes;
	private JLabel lblSedeVisitar;
	private JLabel lblTipoVisita;
	private JLabel lblExpoVisitar; 
	private JLabel lblFechaReserva; 
	private JLabel lblDuracionVisita;
	private JLabel lblGuiasDisp;
	private JLabel lblHoraInicio;
	private JLabel lblHoraFin;
	ArrayList<String> listaEscuela = new ArrayList<String>();
	ArrayList<String> listaSedeVisitar = new ArrayList<String>();
	ArrayList<String> listaTipoVisita = new ArrayList<String>();
	ArrayList<Exposicion> listaExpoVisitar = new ArrayList<Exposicion>();
	ArrayList<String> listaGuia = new ArrayList<String>();
	ArrayList<Exposicion> listaDuracionReserva = new ArrayList<Exposicion>();
	private int duracionExtendida;
	private int duracionResumida;
	Object tipoVisitaSeleccionada;
	Object sedeSeleccionada; 
	Object guiaSeleccionado;
//	app_login login = new app_login();
	private JTextField txtHoraInicio;
	private JTextField txtHoraFin;
	private JScrollPane scrollPaneExpo;
	private JTable tablaExpo;
	private JScrollPane scrollPaneGuias;
	private JTable tablaGuias;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					app_registrarVentaEntradas frame = new app_registrarVentaEntradas();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public app_registrarVentaEntradas() {
		inicializarVariables();
//		registrarVisitas.tomarFechaHoraActual();
	}

	
	public void inicializarVariables() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblEscuela = new JLabel("Escuela:");
		lblEscuela.setBounds(63, 45, 111, 14);
		contentPane.add(lblEscuela);

		lblCantidadVisitantes = new JLabel("Cantidad de visitantes:");
		lblCantidadVisitantes.setBounds(63, 87, 135, 14);
		contentPane.add(lblCantidadVisitantes);

		lblSedeVisitar = new JLabel("Sede a visitar:");
		lblSedeVisitar.setBounds(63, 131, 131, 14);
		contentPane.add(lblSedeVisitar);

		lblTipoVisita = new JLabel("Tipo de Visita:");
		lblTipoVisita.setBounds(63, 177, 79, 14);
		contentPane.add(lblTipoVisita);

		lblExpoVisitar = new JLabel("Exposicion a visitar:");
		lblExpoVisitar.setBounds(63, 217, 135, 14);
		contentPane.add(lblExpoVisitar);

		lblFechaReserva = new JLabel("Fecha de Reserva:");
		lblFechaReserva.setBounds(63, 382, 111, 14);
		contentPane.add(lblFechaReserva);

		lblDuracionVisita = new JLabel("Duracion Aproximada de la visita:");
		lblDuracionVisita.setBounds(62, 429, 209, 14);
		contentPane.add(lblDuracionVisita);

		lblGuiasDisp = new JLabel("Guias Disponibles:");
		lblGuiasDisp.setBounds(62, 464, 147, 14);
		contentPane.add(lblGuiasDisp);

		
		//metodo evento cambia valor campos fecha y hora
		if (txtFechaDesde =! null && txtFechaHasta =! null && txtHoraDesde =! null && txtHoraHasta =! null) {
			if(CONTROL_REGISTRAR_VISITAS.calcularCapacidadMaxima(txtCantidadMaxima.text)) {
				//agregar mensaje de cantidad no valida
				//desahabilitar boton registrar reserva
			} else {
				//quitar mensaje de cantidad no valida
				//habilitar boton registrar reserva
			}
		}
		
		//
		
		
		btnRegistrarReserva = new JButton("Registrar Reserva");
		btnRegistrarReserva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//registrarReserva();
//				cambiarVista();
			}
		});
		btnRegistrarReserva.setBounds(607, 602, 129, 23);
		contentPane.add(btnRegistrarReserva);

		btnCancelarReserva = new JButton("Cancelar");
		btnCancelarReserva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				salir();
			}
		});
		btnCancelarReserva.setBounds(746, 602, 89, 23);
		contentPane.add(btnCancelarReserva);

		comboEscuela = new JComboBox();
		

		comboEscuela.setBounds(269, 41, 111, 22);
		contentPane.add(comboEscuela);
		comboEscuela.requestFocus();

		txtCantVisitantes = new JTextField();
		txtCantVisitantes.setBounds(269, 85, 109, 20);
		contentPane.add(txtCantVisitantes);
		txtCantVisitantes.setColumns(10);

		comboSedeVisitar = new JComboBox();
		comboSedeVisitar.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
			}
		});

//		comboSedeVisitar.addItemListener(new ItemListener() {
//			public void itemStateChanged(ItemEvent e) {
//				
//			}
//		});
		comboSedeVisitar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				CONTROL_REGISTRAR_VISITAS.buscarExpSedeSeleccionada(comboSedeVisitar.getSelectedItem().toString());
			}
		});

		comboSedeVisitar.setBounds(269, 127, 111, 22);
		contentPane.add(comboSedeVisitar);

		comboTipoVisita = new JComboBox();
		

		comboTipoVisita.setBounds(269, 173, 111, 22);
		contentPane.add(comboTipoVisita);

//		dateSelectFechaReserva = new JDateChooser();
//		dateSelectFechaReserva.setBounds(269, 382, 129, 20);
//		contentPane.add(dateSelectFechaReserva);

		txtDuracionVisita = new JTextField();
		txtDuracionVisita.setEnabled(false);
		txtDuracionVisita.setBounds(269, 427, 129, 20);
		contentPane.add(txtDuracionVisita);
		//txtDuracionVisita.setColumns(10);
		
		lblHoraInicio = new JLabel("Desde:");
		lblHoraInicio.setBounds(416, 389, 45, 13);
		contentPane.add(lblHoraInicio);
		
		txtHoraInicio = new JTextField();
		txtHoraInicio.setBounds(451, 386, 96, 19);
		contentPane.add(txtHoraInicio);
		txtHoraInicio.setColumns(10);
		
		lblHoraFin = new JLabel("Hasta:");
		lblHoraFin.setBounds(557, 389, 45, 13);
		contentPane.add(lblHoraFin);
		
		txtHoraFin = new JTextField();
		txtHoraFin.setBounds(592, 386, 96, 19);
		contentPane.add(txtHoraFin);
		txtHoraFin.setColumns(10);
		
		scrollPaneExpo = new JScrollPane(tablaExpo);
		scrollPaneExpo.setBounds(251, 217, 443, 136);
		contentPane.add(scrollPaneExpo);
		
		tablaExpo = new JTable();
		tablaExpo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
	            final JTable jTable= (JTable)e.getSource();
	            final int row = jTable.getSelectedRow();
	            final int column = 3;
	            final String valueInCell = (String)jTable.getValueAt(row, column);
	            txtDuracionVisita.setText(valueInCell);
			}
		});
		tablaExpo.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tablaExpo.setFillsViewportHeight(true);

		scrollPaneExpo.setViewportView(tablaExpo);
		
		scrollPaneGuias = new JScrollPane();
		scrollPaneGuias.setBounds(245, 497, 443, 71);
		contentPane.add(scrollPaneGuias);
		
		tablaGuias = new JTable();
		scrollPaneGuias.setViewportView(tablaGuias);
		
		JLabel lblGuiasASeleccionar = new JLabel("Seleccione ... gu�a/s");
		lblGuiasASeleccionar.setBounds(245, 464, 147, 14);
		contentPane.add(lblGuiasASeleccionar);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"14:00"}));
		comboBox.setBounds(451, 413, 96, 22);
		contentPane.add(comboBox);
		
        
		cargarDatos();
		cargarCombos();
	}

	protected void registrarReserva() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	    System.out.println("dd/MM/yyyy HH:mm:ss-> "+dtf.format(LocalDateTime.now()));
	    DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
//	    String date = formatter.format(dateSelectFechaReserva.getDate());
//	    System.out.println(formatter.format(dateSelectFechaReserva));
	    
		/*: Registra la reserva con estado Pendiente de Confirmaci�n, generando un n�mero �nico, empleado 
			que registr�, fecha y hora de creaci�n, fecha y hora de reserva, duraci�n estimada de la visita, exposiciones a 
			visitar, escuela, cantidad de visitantes y gu�as asignados para la visita.*/
		// this.login.getUsuarioLogueado(), fechaCreacion, dateSelectFechaReserva, txtHoraInicio, txtHoraFin,txtDuracionVisita.getText(),comboExpoVisitar.getSelectedItem(), comboEscuela.getSelectedItem(),txtCantVisitantes.getText(),this.guiaSeleccionado,this.sedeSeleccionada
		// this.login.getUsuarioLogueado(), txtDuracionVisita.getText(), comboExpoVisitar.getSelectedItem(), comboEscuela.getSelectedItem(),txtCantVisitantes.getText(),this.guiaSeleccionado,this.sedeSeleccionada
//		this.guiaSeleccionado = comboGuiasDisp.getSelectedItem();
	    //empleado, fechaCreacion, fechaReserva, horaReservaInicio, horaReservaFin, duracionEstimadaVisita, exposiciones, escuela, cantidadVisitantes, guias, sede
	    String usuarioLogueado = "vparis";
//	    System.out.println("Usuario Logueado: " + usuarioLogueado);
//	    System.out.println("Fecha actual " +  dtf.format(LocalDateTime.now()));
//	    System.out.println("Fecha Seleccionada " + date);
//	    System.out.println("Desde: " + Integer.parseInt(txtHoraInicio.getText()));
//	    System.out.println("Hasta: " + Integer.parseInt(txtHoraFin.getText()));
//	    System.out.println("Duracion de la visita: " +Integer.parseInt(txtDuracionVisita.getText()));
//	    System.out.println("Expo a visitar: " + comboExpoVisitar.getSelectedItem().toString());
//	    System.out.println("Escuela seleccionada:" + comboEscuela.getSelectedItem().toString());
//	    System.out.println("Cantidad de visitantes: " + Integer.parseInt(txtCantVisitantes.getText()));
//	    System.out.println("Guia seleccionado: " + comboGuiasDisp.getSelectedItem().toString());
//	    System.out.println("Sede seleccionada: " + comboSedeVisitar.getSelectedItem().toString());
	     
		/*registrarVisitas.RegistrarReserva(usuarioLogueado, dtf.format(LocalDateTime.now()), date, Integer.parseInt(txtHoraInicio.getText()), Integer.parseInt(txtHoraFin.getText()),
										  Integer.parseInt(txtDuracionVisita.getText()),comboExpoVisitar.getSelectedItem().toString(), 
										  comboEscuela.getSelectedItem().toString(),Integer.parseInt(txtCantVisitantes.getText()),
										  comboGuiasDisp.getSelectedItem().toString(),comboSedeVisitar.getSelectedItem().toString());*/
	}
	
	/*protected void exposicionSedeSeleccionada() {
		this.sedeSeleccionada = comboSedeVisitar.getSelectedItem();

		registrarVisitas.buscarExpSedeSeleccionada(this.sedeSeleccionada);
		listaExpoVisitar = registrarVisitas.getSede().getListaExposicion();

		tablaExpo.removeAll();
		for (int i = 0; i < listaExpoVisitar.size(); i++) {
			tablaExpo.addItem("" + listaExpoVisitar.get(i).getNombre() + " - "
					+ listaExpoVisitar.get(i).getPublicoDestino().getNombre() + " - " + " Atenci�n de: "
					+ listaExpoVisitar.get(i).getHoraApertura().getHours() + " a "
					+ listaExpoVisitar.get(i).getHoraCierre().getHours() + " hs.");
		}
	}*/

	private void cargarCombos() {

		List<Escuela> escuelas = CONTROL_REGISTRAR_VISITAS.buscarEscuela();
		
		for (Escuela escuela : escuelas) {
			comboEscuela.addItem(escuela.getNombre());
		}
		
		List<Sede> sedes = CONTROL_REGISTRAR_VISITAS.buscarSede();
		
		for (Sede sede : sedes) {
			comboSedeVisitar.addItem(sede.getNombre());
		}
		
		List<TipoVisita> tipoVisitas = CONTROL_REGISTRAR_VISITAS.buscarTipoVisita();;
		
		for (TipoVisita tipoVisita : tipoVisitas) {
			comboTipoVisita.addItem(tipoVisita.getNombre());
		}
		
		
		
	}
	


	protected void salir() {
//		this.dispose();
//		app_menuPrincipal menuPrincipal = new app_menuPrincipal();
//		menuPrincipal.setVisible(true);
	}
	
private static void cargarDatos() {
		
		em.getTransaction().begin();
		
		Obra o = new Obra("Mona Lisa", 60, 30);
		Obra obraVME = new Obra("El wacho bonfi", 90, 45);
		DetalleExposicion de = new DetalleExposicion("1B", o);
		DetalleExposicion detalleVM = new DetalleExposicion("1A", obraVME);
		
		Sede sedeOficial = new Sede();
		sedeOficial.setNombre("Sede Oficial");
		sedeOficial.setCantidadMaximaVisitante(2);
		sedeOficial.setCantMaxPorGuia(1);
		
		TipoExposicion temporal = new TipoExposicion("Temporal", "Es temporal");
		TipoExposicion perma = new TipoExposicion("Permanente", "Es permanente");
		
		TipoVisita tipoVisitaCompleta = new TipoVisita("Completa");
		TipoVisita tipoVisitaPorExposicion = new TipoVisita("Por exposici�n");
		
		Exposicion villaNuevaExpo = new Exposicion("Exposicion 1 ", temporal);
		villaNuevaExpo.setFechaHoraInicio(new Date());
		villaNuevaExpo.setFechaHoraFin(new Date());
		villaNuevaExpo.setHoraApertura(LocalTime.now());
		villaNuevaExpo.setHoraCierre(LocalTime.of(23, 40, 0));
		villaNuevaExpo.getDetalleExposicion().add(de);
		
		Exposicion villaMariExpo = new Exposicion("Exposicion 2", temporal);
		villaMariExpo.setFechaHoraInicio(new Date());
		villaMariExpo.setFechaHoraFin(new Date());
		villaMariExpo.setHoraApertura(LocalTime.now());
		villaMariExpo.setHoraCierre(LocalTime.of(23, 40, 0));
		villaMariExpo.getDetalleExposicion().add(de);
		villaMariExpo.getDetalleExposicion().add(detalleVM);

		Exposicion cordobaExpo = new Exposicion("Exposicion 3", perma);
		cordobaExpo.setFechaHoraInicio(new Date());
		cordobaExpo.setFechaHoraFin(new Date());
		cordobaExpo.setHoraApertura(LocalTime.now());
		cordobaExpo.setHoraCierre(LocalTime.of(23, 40, 0));
		cordobaExpo.getDetalleExposicion().add(de);
		
		PublicoDestino pd = new PublicoDestino("Jubilados");
		villaNuevaExpo.getPublicos().add(pd);
		cordobaExpo.getPublicos().add(pd);
		
		Escuela escuela = new Escuela("Trinitarios");
		
		sedeOficial.agregarExposicion(villaNuevaExpo);
		sedeOficial.agregarExposicion(cordobaExpo);
		sedeOficial.agregarExposicion(villaMariExpo);
		
		em.persist(pd);
		em.persist(sedeOficial);
		em.persist(escuela);
		em.persist(de);
		em.persist(tipoVisitaCompleta);
		em.persist(tipoVisitaPorExposicion);
		em.getTransaction().commit();
		
		em.close();
//		PersistenceManager.close();
		
//		em.persist(o);
//		em.persist(de);
		
//		em.persist(tipovisita);
//		em.persist(horarioEmpleado);
////	em.persist(empleado);
//		
//		asignacionVisita.setEmpleado(empleado);
//		empleado.setAsignacionVisita(asignacionVisita);
//		em.persist(asignacionVisita);
//		em.persist(usuario);
//		em.persist(sesion);
//		em.persist(reservaVisita);
//		reservaVisita.setSede(sedeOficial);
//		em.persist(estado);
//		em.persist(cambioEstado);
//		em.getTransaction().commit();
//		
		
		
		
//		Cargo cargo = new Cargo("Guia", "Es un guia de sede");
//		HorarioEmpleado horarioEmpleado = new HorarioEmpleado(new Date(), new Date());
//		ReservaVisita reservaVisita = new ReservaVisita(30, 35, 40, 60, new Date(), new Date(), 1, sedeOficial);
//		Empleado empleado = new Empleado();
//		empleado.setNombre("Valentin");
//		empleado.setApellido("Paris");
//		empleado.setCargo(cargo);
//		AsignacionVisita asignacionVisita = new AsignacionVisita();
//		asignacionVisita.setFechaHoraInicio(new Date());
//		asignacionVisita.setFechaHoraFin(new Date());
//		asignacionVisita.setReservaVisita(reservaVisita);
//		
//		Usuario usuario = new Usuario("valentinp", "1234", empleado);
//		Sesion sesion = new Sesion(new Date(), new Date(), usuario);
//		
//		Estado estado = new Estado("ambito", "aceptado", "Estado aceptado");
//		CambioEstado cambioEstado = new CambioEstado(new Date(),new Date(), estado, reservaVisita);
		
		
		
	}
}
