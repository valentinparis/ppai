package ar.utn.ppai.disenio.vista;

import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import ar.utn.ppai.disenio.controlador.ControlRegistrarVisitas;
import ar.utn.ppai.disenio.modelo.DetalleExposicion;
import ar.utn.ppai.disenio.modelo.Escuela;
import ar.utn.ppai.disenio.modelo.Exposicion;
import ar.utn.ppai.disenio.modelo.Obra;
import ar.utn.ppai.disenio.modelo.PublicoDestino;
import ar.utn.ppai.disenio.modelo.Sede;
import ar.utn.ppai.disenio.modelo.TipoExposicion;
import ar.utn.ppai.disenio.modelo.TipoVisita;
import ar.utn.ppai.disenio.modelo.infra.PersistenceManager;
import ar.utn.ppai.disenio.modelo.infra.repositories.EscuelaRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.ExposicionRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.SedeRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.TipoVisitaRepository;
import ar.utn.ppai.disenio.modelo.infra.repositories.impl.EscuelaRepositoryJpa;
import ar.utn.ppai.disenio.modelo.infra.repositories.impl.ExposicionRepositoryJpa;
import ar.utn.ppai.disenio.modelo.infra.repositories.impl.SedeRepositoryJpa;
import ar.utn.ppai.disenio.modelo.infra.repositories.impl.TipoVisitaRepositoryJpa;

public class Prueba {
	
	private static final EntityManager em = PersistenceManager.getInstance().entityManager();
	private static final ExposicionRepository EXPOSICION_REPOSITORY = new ExposicionRepositoryJpa(PersistenceManager.getInstance().entityManager());
	private static final EscuelaRepository ESCUELA_REPOSITORY= new EscuelaRepositoryJpa(PersistenceManager.getInstance().entityManager());
	private static final SedeRepository SEDE_REPOSITORY = new SedeRepositoryJpa(PersistenceManager.getInstance().entityManager());
	private static final TipoVisitaRepository TIPO_VISITA_REPOSITORY = new TipoVisitaRepositoryJpa(PersistenceManager.getInstance().entityManager());
	private static final ControlRegistrarVisitas CONTROL_REGISTRAR_VISITAS = new ControlRegistrarVisitas(ESCUELA_REPOSITORY, SEDE_REPOSITORY, TIPO_VISITA_REPOSITORY);
	public static void main(String[] args) {
		
		try {
			
			cargarDatos();
			List<Escuela> escuelas = ESCUELA_REPOSITORY.findAll();
			
			for (Escuela escuela : escuelas) {
				System.out.println("escuelas: " + escuela.getNombre());
			}
			
			
//			List<Exposicion> exposiciones = EXPOSICION_REPOSITORY.findAll();
			List<Sede> sedes = SEDE_REPOSITORY.findAll();
			
//			for (Sede sede : sedes) {
//				System.out.println("Sede: " + sede.getNombre());
//				CONTROL_REGISTRAR_VISITAS.buscarExpSedeSeleccionada(sede);
//				CONTROL_REGISTRAR_VISITAS.calcularDuracionReserva(sede);
//				CONTROL_REGISTRAR_VISITAS.buscarGuiasDispFechaReserva(sede);
//			}
			
			Sede sedePrueba = sedes.get(0);
			List<Exposicion> exposSelecc = CONTROL_REGISTRAR_VISITAS.buscarExpSedeSeleccionada(sedePrueba);
			CONTROL_REGISTRAR_VISITAS.calcularDuracionReserva(sedePrueba, exposSelecc);
			
			
			
			
//			CONTROL_REGISTRAR_VISITAS.buscarGuiasDispFechaReserva(sedePrueba);
			
//			List<TipoVisita> tipoVisitas = TIPO_VISITA_REPOSITORY.findAll();
			
//			exposiciones.forEach(System.out::println);
//			escuelas.forEach(System.out::println);
			sedes.forEach(System.out::println);
//			tipoVisitas.forEach(System.out::println);
			
		} catch (Exception e) {
			e.printStackTrace();
			em.close();
			PersistenceManager.close();
			
		}
		
		
		em.close();
		PersistenceManager.close();
	}
	
	private static void cargarDatos() {
		
		em.getTransaction().begin();
		
		Obra o = new Obra("Mona Lisa", 60, 30);
		Obra obraVME = new Obra("El wacho bonfi", 90, 45);
		DetalleExposicion de = new DetalleExposicion("1B", o);
		DetalleExposicion detalleVM = new DetalleExposicion("1A", obraVME);
		
		Sede sedeOficial = new Sede();
		sedeOficial.setNombre("Sede Oficial");
		sedeOficial.setCantidadMaximaVisitante(2);
		sedeOficial.setCantMaxPorGuia(1);
		
		TipoExposicion temporal = new TipoExposicion("Temporal", "Es temporal");
		TipoExposicion perma = new TipoExposicion("Permanente", "Es permanente");
		
		TipoVisita tipoVisitaCompleta = new TipoVisita("Completa");
		TipoVisita tipoVisitaPorExposicion = new TipoVisita("Por exposición");
		
		Exposicion villaNuevaExpo = new Exposicion("Exposicion 1 ", temporal);
		villaNuevaExpo.setFechaHoraInicio(new Date());
		villaNuevaExpo.setFechaHoraFin(new Date());
		villaNuevaExpo.setHoraApertura(LocalTime.now());
		villaNuevaExpo.setHoraCierre(LocalTime.of(23, 40, 0));
		villaNuevaExpo.getDetalleExposicion().add(de);
		
		Exposicion villaMariExpo = new Exposicion("Exposicion 2", temporal);
		villaMariExpo.setFechaHoraInicio(new Date());
		villaMariExpo.setFechaHoraFin(new Date());
		villaMariExpo.setHoraApertura(LocalTime.now());
		villaMariExpo.setHoraCierre(LocalTime.of(23, 40, 0));
		villaMariExpo.getDetalleExposicion().add(de);
		villaMariExpo.getDetalleExposicion().add(detalleVM);

		Exposicion cordobaExpo = new Exposicion("Exposicion 3", perma);
		cordobaExpo.setFechaHoraInicio(new Date());
		cordobaExpo.setFechaHoraFin(new Date());
		cordobaExpo.setHoraApertura(LocalTime.now());
		cordobaExpo.setHoraCierre(LocalTime.of(23, 40, 0));
		cordobaExpo.getDetalleExposicion().add(de);
		
		PublicoDestino pd = new PublicoDestino("Jubilados");
		villaNuevaExpo.getPublicos().add(pd);
		cordobaExpo.getPublicos().add(pd);
		
		Escuela escuela = new Escuela("Trinitarios");
		
		sedeOficial.agregarExposicion(villaNuevaExpo);
		sedeOficial.agregarExposicion(cordobaExpo);
		sedeOficial.agregarExposicion(villaMariExpo);
		
		em.persist(pd);
		em.persist(sedeOficial);
		em.persist(escuela);
		em.persist(de);
		em.getTransaction().commit();
		
		em.close();
//		PersistenceManager.close();
		
//		em.persist(o);
//		em.persist(de);
		
//		em.persist(tipovisita);
//		em.persist(horarioEmpleado);
////	em.persist(empleado);
//		
//		asignacionVisita.setEmpleado(empleado);
//		empleado.setAsignacionVisita(asignacionVisita);
//		em.persist(asignacionVisita);
//		em.persist(usuario);
//		em.persist(sesion);
//		em.persist(reservaVisita);
//		reservaVisita.setSede(sedeOficial);
//		em.persist(estado);
//		em.persist(cambioEstado);
//		em.getTransaction().commit();
//		
		
		
		
//		Cargo cargo = new Cargo("Guia", "Es un guia de sede");
//		HorarioEmpleado horarioEmpleado = new HorarioEmpleado(new Date(), new Date());
//		ReservaVisita reservaVisita = new ReservaVisita(30, 35, 40, 60, new Date(), new Date(), 1, sedeOficial);
//		Empleado empleado = new Empleado();
//		empleado.setNombre("Valentin");
//		empleado.setApellido("Paris");
//		empleado.setCargo(cargo);
//		AsignacionVisita asignacionVisita = new AsignacionVisita();
//		asignacionVisita.setFechaHoraInicio(new Date());
//		asignacionVisita.setFechaHoraFin(new Date());
//		asignacionVisita.setReservaVisita(reservaVisita);
//		
//		Usuario usuario = new Usuario("valentinp", "1234", empleado);
//		Sesion sesion = new Sesion(new Date(), new Date(), usuario);
//		
//		Estado estado = new Estado("ambito", "aceptado", "Estado aceptado");
//		CambioEstado cambioEstado = new CambioEstado(new Date(),new Date(), estado, reservaVisita);
		
		
		
	}
}
