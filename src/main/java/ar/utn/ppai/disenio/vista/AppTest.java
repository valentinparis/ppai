package ar.utn.ppai.disenio.vista;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import ar.utn.ppai.disenio.controlador.ControlRegistrarVisitas;
import ar.utn.ppai.disenio.modelo.AsignacionVisita;
import ar.utn.ppai.disenio.modelo.CambioEstado;
import ar.utn.ppai.disenio.modelo.Cargo;
import ar.utn.ppai.disenio.modelo.DetalleExposicion;
import ar.utn.ppai.disenio.modelo.Empleado;
import ar.utn.ppai.disenio.modelo.Escuela;
import ar.utn.ppai.disenio.modelo.Estado;
import ar.utn.ppai.disenio.modelo.Exposicion;
import ar.utn.ppai.disenio.modelo.HorarioEmpleado;
import ar.utn.ppai.disenio.modelo.Obra;
import ar.utn.ppai.disenio.modelo.PublicoDestino;
import ar.utn.ppai.disenio.modelo.ReservaVisita;
import ar.utn.ppai.disenio.modelo.Sede;
import ar.utn.ppai.disenio.modelo.Sesion;
import ar.utn.ppai.disenio.modelo.TipoExposicion;
import ar.utn.ppai.disenio.modelo.TipoVisita;
import ar.utn.ppai.disenio.modelo.Usuario;
import ar.utn.ppai.disenio.modelo.infra.PersistenceManager;

public class AppTest {
	
	private static final EntityManager em = PersistenceManager.getInstance().entityManager();
	
	public static void main(String[] args) {
		
		try {
			
			cargarDatos();	
			
		} catch (Exception e) {
			e.printStackTrace();
			em.close();
			PersistenceManager.close();
			
		}

	}

	private static void cargarDatos() {
		
		em.getTransaction().begin();
		
		Sede sedeOficial = new Sede();
		sedeOficial.setNombre("Sede Oficial");
		sedeOficial.setCantidadMaximaVisitante(2);
		sedeOficial.setCantMaxPorGuia(1);
		
		TipoExposicion temporal = new TipoExposicion("Temporal", "Es temporal");
		TipoExposicion perma = new TipoExposicion("Permanente", "Es permanente");
		
		Exposicion villaNuevaExpo = new Exposicion("Villa Nueva Expo", temporal);
		villaNuevaExpo.setFechaHoraInicio(new Date());
		villaNuevaExpo.setFechaHoraFin(new Date());
		villaNuevaExpo.setHoraApertura(LocalTime.now());
		villaNuevaExpo.setHoraCierre(LocalTime.of(23, 40, 0));

		Exposicion cordobaExpo = new Exposicion("C�rdoba Expo", perma);
		cordobaExpo.setFechaHoraInicio(new Date());
		cordobaExpo.setFechaHoraFin(new Date());
		cordobaExpo.setHoraApertura(LocalTime.now());
		cordobaExpo.setHoraCierre(LocalTime.of(23, 40, 0));
		
		PublicoDestino pd = new PublicoDestino("Jubilados");
		villaNuevaExpo.getPublicos().add(pd);
		cordobaExpo.getPublicos().add(pd);
		
		sedeOficial.getExposiciones().add(villaNuevaExpo);
		sedeOficial.getExposiciones().add(cordobaExpo);
		
//		em.persist(o);
//		em.persist(de);
//		em.persist(escuela);
//		em.persist(tipovisita);
//		em.persist(horarioEmpleado);
////	em.persist(empleado);
//		
//		asignacionVisita.setEmpleado(empleado);
//		empleado.setAsignacionVisita(asignacionVisita);
//		em.persist(asignacionVisita);
//		em.persist(usuario);
//		em.persist(sesion);
//		em.persist(reservaVisita);
//		reservaVisita.setSede(sedeOficial);
//		em.persist(estado);
//		em.persist(cambioEstado);
//		em.getTransaction().commit();
//		
//		Obra o = new Obra("Mona Lisa", 60, 30);
//		DetalleExposicion de = new DetalleExposicion("1B", o);
//		Escuela escuela = new Escuela("Trinitarios");
//		TipoVisita tipovisita = new TipoVisita("Visita Guiada");
//		Cargo cargo = new Cargo("Guia", "Es un guia de sede");
//		HorarioEmpleado horarioEmpleado = new HorarioEmpleado(new Date(), new Date());
//		ReservaVisita reservaVisita = new ReservaVisita(30, 35, 40, 60, new Date(), new Date(), 1, sedeOficial);
//		Empleado empleado = new Empleado();
//		empleado.setNombre("Valentin");
//		empleado.setApellido("Paris");
//		empleado.setCargo(cargo);
//		AsignacionVisita asignacionVisita = new AsignacionVisita();
//		asignacionVisita.setFechaHoraInicio(new Date());
//		asignacionVisita.setFechaHoraFin(new Date());
//		asignacionVisita.setReservaVisita(reservaVisita);
//		
//		Usuario usuario = new Usuario("valentinp", "1234", empleado);
//		Sesion sesion = new Sesion(new Date(), new Date(), usuario);
//		
//		Estado estado = new Estado("ambito", "aceptado", "Estado aceptado");
//		CambioEstado cambioEstado = new CambioEstado(new Date(),new Date(), estado, reservaVisita);
		
		em.persist(pd);
		em.persist(sedeOficial);
		em.getTransaction().commit();
		
		em.close();
		PersistenceManager.close();
		
	}
	
	
	
}
