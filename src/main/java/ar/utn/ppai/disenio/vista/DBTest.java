package ar.utn.ppai.disenio.vista;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import ar.utn.ppai.disenio.modelo.DetalleExposicion;
import ar.utn.ppai.disenio.modelo.Escuela;
import ar.utn.ppai.disenio.modelo.Exposicion;
import ar.utn.ppai.disenio.modelo.Obra;
import ar.utn.ppai.disenio.modelo.PublicoDestino;
import ar.utn.ppai.disenio.modelo.Sede;
import ar.utn.ppai.disenio.modelo.TipoExposicion;

public class DBTest {

	private static EntityManagerFactory emf;
	private static EntityManager manager;
	
	public static void main(String[] args) {
		
		emf = Persistence.createEntityManagerFactory("Persistencia");
		manager = emf.createEntityManager(); 
		
		Sede sede = new Sede("Sede Villa Nueva", 150, 30);
		
		manager.getTransaction().begin();
		manager.persist(sede);
		manager.getTransaction().commit();
		
		TipoExposicion te = new TipoExposicion("Corta", "Exposicion corta");
		
		manager.getTransaction().begin();
		manager.persist(te);
		manager.getTransaction().commit();
		
		Exposicion e = new Exposicion("Exposicion 2", te);		
		
		manager.getTransaction().begin();
		manager.persist(e);
		manager.getTransaction().commit();
		
		PublicoDestino pd = new PublicoDestino("Estudiantes");
		
		manager.getTransaction().begin();
		manager.persist(pd); 
		manager.getTransaction().commit();
		
		Obra o = new Obra("Mona Lisa", 60, 30);
		
		manager.getTransaction().begin();
		manager.persist(o); 
		manager.getTransaction().commit();
		
		DetalleExposicion de = new DetalleExposicion("1A", o);
		
		manager.getTransaction().begin();
		manager.persist(de); 
		manager.getTransaction().commit();
		
		Escuela escuela = new Escuela("Trinitarios");
		
		manager.getTransaction().begin();
		manager.persist(escuela); 
		manager.getTransaction().commit();
		
		imprimir();
		
		
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	private static void imprimir() {
		
		List<Sede> listaSede = (List<Sede>) manager.createQuery("from Sede").getResultList();
		for (Sede s : listaSede) { // se itera sobre la lista persistida para mostrar los valores que necesitemos
			System.out.println(s.toString()); 
		}
		
		List<Exposicion> listaExp= (List<Exposicion>) manager.createQuery("from Exposicion").getResultList();
		for (Exposicion le : listaExp) { 
			System.out.println(le.toString()); 
		}
		
		List<PublicoDestino> listaPD= (List<PublicoDestino>) manager.createQuery("from PublicoDestino").getResultList();
		for (PublicoDestino pd : listaPD) { 
			System.out.println(pd.toString()); 
		}
		
		List<Obra> listaObra= (List<Obra>) manager.createQuery("from Obra").getResultList();
		for (Obra o : listaObra) { 
			System.out.println(o.toString()); 
		}
		
		List<DetalleExposicion> listaDE= (List<DetalleExposicion>) manager.createQuery("from DetalleExposicion").getResultList();
		for (DetalleExposicion de : listaDE) { 
			System.out.println(de.toString()); 
		}
		
		List<Escuela> listaEscuela= (List<Escuela>) manager.createQuery("from Escuela").getResultList();
		for (Escuela esc : listaEscuela) { 
			System.out.println(esc.toString()); 
		}
		
	}

}
