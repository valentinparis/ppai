package ar.utn.ppai.disenio.modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "empleado")
public class Empleado {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idempleado")
	private int id;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "apellido")
	private String apellido;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idcargo")
	private Cargo cargo;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinColumn(name = "id_horario_empleado")
	private List<HorarioEmpleado> horarioEmpleado;
	
	@OneToOne(mappedBy = "empleado", fetch = FetchType.LAZY, optional = false)
	private AsignacionVisita asignacionVisita;
		
	public Empleado() {}
	
	public Empleado(String nombre, String apellido, Cargo cargo) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.cargo = cargo;
		this.horarioEmpleado = new ArrayList<HorarioEmpleado>();
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public AsignacionVisita getAsignacionVisita() {
		return asignacionVisita;
	}

	public void setAsignacionVisita(AsignacionVisita asignacionVisita) {
		this.asignacionVisita = asignacionVisita;
	}
	
	@Override
	public String toString() {
		return "Empleado [nombre=" + nombre + ", apellido=" + apellido + ", cargo=" + cargo +"]";
	}

	public boolean esDeSede(Sede sede) {
		return true;
	}
	
	public List<String> getGuiaDispEnHorario(Sede sede) {
		List<String> empleados = new ArrayList<String>();
		this.cargo.esGuia();
		this.esDeSede(sede);
		if(horarioEmpleado.size() >0 && horarioEmpleado != null) {
			for (HorarioEmpleado horarioEmpleado : horarioEmpleado) {
				horarioEmpleado.getFechaHoraIngreso().before(new Date());
			}
		} else if (asignacionVisita.esAsignacionParaFechaHora()) {
			String apellidoNombreEmpleado = this.getApellido() + "," + this.getNombre();
			empleados.add(apellidoNombreEmpleado);
		}
		
		return empleados;
	}
	
}
