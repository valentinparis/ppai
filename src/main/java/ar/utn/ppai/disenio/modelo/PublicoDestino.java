package ar.utn.ppai.disenio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "publicodestino")
public class PublicoDestino {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idpublicodestino")
	private int id;
	
	@Column(name = "nombre")
	private String nombre;
	
	/*
	@ManyToOne
	@JoinColumn(name = "idexposicion")
	private Exposicion exposicion;
	*/
	
	public PublicoDestino() {}
	
	
	
	public PublicoDestino(String nombre) {
		this.nombre = nombre;
	}



	/*
	public PublicoDestino(String nombre,Exposicion exposicion) {
		this.nombre = nombre;
		this.exposicion = exposicion;
	}
	*/

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	@Override
	public String toString() {
		return "PublicoDestino [id=" + id + ", nombre=" + nombre + "]";
	}

	
	/*
	public Exposicion getExposicion() {
		return exposicion;
	}

	public void setExposicion(Exposicion exposicion) {
		this.exposicion = exposicion;
	}
	*/
	
	
	
}
