package ar.utn.ppai.disenio.modelo.infra.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import ar.utn.ppai.disenio.modelo.Exposicion;
import ar.utn.ppai.disenio.modelo.infra.PersistenceManager;
import ar.utn.ppai.disenio.modelo.infra.repositories.ExposicionRepository;

public class ExposicionRepositoryJpa implements ExposicionRepository {
	
	private final EntityManager em;
	
	public ExposicionRepositoryJpa(EntityManager em) {
		this.em = em;
	}

	@Override
	public List<Exposicion> findAll() {
		List<Exposicion> exposiciones = new ArrayList<Exposicion>();
		
		try {
			List<Exposicion> resultSet = em.createQuery("FROM Exposicion").getResultList();
			
			exposiciones.addAll(resultSet);
			
			em.close();
//			PersistenceManager.close();
		} catch (Exception e) {
			e.printStackTrace();
			em.close();
			PersistenceManager.close();
		}
		
		return exposiciones;
	}
	
	
	
}
