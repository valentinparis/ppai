package ar.utn.ppai.disenio.modelo.infra.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import ar.utn.ppai.disenio.modelo.Sede;
import ar.utn.ppai.disenio.modelo.infra.PersistenceManager;
import ar.utn.ppai.disenio.modelo.infra.repositories.SedeRepository;

public class SedeRepositoryJpa implements SedeRepository{

	private final EntityManager em;
	
	public SedeRepositoryJpa(EntityManager em) {
		this.em = em;
	}
	
	@Override
	public List<Sede> findAll() {
		
		List<Sede> sedes = new ArrayList<Sede>();
		
		try {
			List<Sede> resultSet = em.createQuery("FROM Sede").getResultList();
			
			sedes.addAll(resultSet);
			
			em.close();
//			PersistenceManager.close();
		} catch (Exception e) {
			e.printStackTrace();
			em.close();
			PersistenceManager.close();
		}
		
		return sedes;
	}

}
