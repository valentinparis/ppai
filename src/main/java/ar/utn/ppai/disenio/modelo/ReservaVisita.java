package ar.utn.ppai.disenio.modelo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "reservavisita")
public class ReservaVisita {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idreservavisita")
	private int id;
	
	@Column(name = "cantidadAlumnos")
	private int cantidadAlumnos;
	
	@Column(name = "cantidadAlumnosConfirmada")
	private int cantidadAlumnosConfirmada;
	
	@Column(name = "cantidadMaxima")
	private int cantidadMaxima;
	
	@Column(name = "duracionEstimada")
	private int duracionEstimada;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fechaHoraCreacion")
	private Date fechaHoraCreacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fechaHoraReserva")
	private Date fechaHoraReserva;
	
	@Column(name = "nroReserva")
	private int nroReserva;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idsede")
	private Sede sede;
	
	public ReservaVisita() {}
	
	
	
	public ReservaVisita(int cantidadAlumnos, int cantidadAlumnosConfirmada, int cantidadMaxima,
			int duracionEstimada, Date fechaHoraCreacion, Date fechaHoraReserva, int nroReserva, Sede sede) {
		
		this.cantidadAlumnos = cantidadAlumnos;
		this.cantidadAlumnosConfirmada = cantidadAlumnosConfirmada;
		this.cantidadMaxima = cantidadMaxima;
		this.duracionEstimada = duracionEstimada;
		this.fechaHoraCreacion = fechaHoraCreacion;
		this.fechaHoraReserva = fechaHoraReserva;
		this.nroReserva = nroReserva;
		this.sede = sede;
	}



	public int getNroReserva() {
		return nroReserva;
	}

	public void setNroReserva(int nroReserva) {
		this.nroReserva = nroReserva;
	}

	public Date getFechaHoraReserva() {
		return fechaHoraReserva;
	}

	public void setFechaHoraReserva(Date fechaHoraReserva) {
		this.fechaHoraReserva = fechaHoraReserva;
	}

	public int getCantidadMaxima() {
		return cantidadMaxima;
	}

	public void setCantidadMaxima(int cantidadMaxima) {
		this.cantidadMaxima = cantidadMaxima;
	}

	public int getDuracionEstimada() {
		return duracionEstimada;
	}

	public void setDuracionEstimada(int duracionEstimada) {
		this.duracionEstimada = duracionEstimada;
	}

	public int getCantidadAlumnos() {
		return cantidadAlumnos;
	}

	public void setCantidadAlumnos(int cantidadAlumnos) {
		this.cantidadAlumnos = cantidadAlumnos;
	}

	public int getCantidadAlumnosConfirmada() {
		return cantidadAlumnosConfirmada;
	}

	public void setCantidadAlumnosConfirmada(int cantidadAlumnosConfirmada) {
		this.cantidadAlumnosConfirmada = cantidadAlumnosConfirmada;
	}

	public Date getFechaHoraCreacion() {
		return fechaHoraCreacion;
	}

	public void setFechaHoraCreacion(Date fechaHoraCreacion) {
		this.fechaHoraCreacion = fechaHoraCreacion;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

	@Override
	public String toString() {
		return "ReservaVisita [cantidadAlumnos=" + cantidadAlumnos + ", cantidadAlumnosConfirmada="
				+ cantidadAlumnosConfirmada + ", cantidadMaxima=" + cantidadMaxima + ", duracionEstimada="
				+ duracionEstimada + ", fechaHoraCreacion=" + fechaHoraCreacion.getTime() + ", fechaHoraReserva="
				+ fechaHoraReserva.getTime() + ", nroReserva=" + nroReserva + ", sede=" + sede + "]";
	}

	public void crearAsigGuia() {}
	
	public void crearCambioEstado() {}
	
	public void obtenerAlumnoEnReserva() {}
	
}
