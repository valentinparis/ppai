package ar.utn.ppai.disenio.modelo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cambioestado")
public class CambioEstado {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idcambioestado")
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fechaHoraFin")
	private Date fechaHoraFin;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fechaHoraInicio")
	private Date fechaHoraInicio;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idestado")
	private Estado estado;
	
	@ManyToOne
	@JoinColumn(name = "idreservavisita")
	private ReservaVisita reservaVisita; 
	
	public CambioEstado() {}
	
	public CambioEstado(Date fechaHoraFin, Date fechaHoraInicio, Estado estado, ReservaVisita reservaVisita) {
		this.fechaHoraFin = fechaHoraFin;
		this.fechaHoraInicio = fechaHoraInicio;
		this.estado = estado;
		this.reservaVisita = reservaVisita;
	}

	public Date getFechaHoraFin() {
		return fechaHoraFin;
	}
	public void setFechaHoraFin(Date fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}
	public Date getFechaHoraInicio() {
		return fechaHoraInicio;
	}
	public void setFechaHoraInicio(Date fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public ReservaVisita getReservaVisita() {
		return reservaVisita;
	}

	public void setReservaVisita(ReservaVisita reservaVisita) {
		this.reservaVisita = reservaVisita;
	}

	@Override
	public String toString() {
		return "CambioEstado [fechaHoraFin=" + fechaHoraFin.getTime() + ", fechaHoraInicio=" + fechaHoraInicio.getTime() + ", estado="
				+ estado + ", reservaVisita=" + reservaVisita + "]";
	}
	
}
