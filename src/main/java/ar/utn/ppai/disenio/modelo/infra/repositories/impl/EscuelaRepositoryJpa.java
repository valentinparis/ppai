package ar.utn.ppai.disenio.modelo.infra.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import ar.utn.ppai.disenio.modelo.Escuela;
import ar.utn.ppai.disenio.modelo.infra.PersistenceManager;
import ar.utn.ppai.disenio.modelo.infra.repositories.EscuelaRepository;

public class EscuelaRepositoryJpa implements EscuelaRepository{
	
	private final EntityManager em;
	
	public EscuelaRepositoryJpa(EntityManager em) {
		this.em = em;
	}
	
	@Override
	public List<Escuela> findAll() {
		
		List<Escuela> escuelas = new ArrayList<Escuela>();
		
		try {
			List<Escuela> resultSet = em.createQuery("FROM Escuela").getResultList();
			
			escuelas.addAll(resultSet);
			
			em.close();
//			PersistenceManager.close();
		} catch (Exception e) {
			e.printStackTrace();
			em.close();
			PersistenceManager.close();
		}
		
		return escuelas;
	}

}
