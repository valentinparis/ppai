package ar.utn.ppai.disenio.modelo.infra.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import ar.utn.ppai.disenio.modelo.TipoVisita;
import ar.utn.ppai.disenio.modelo.infra.PersistenceManager;
import ar.utn.ppai.disenio.modelo.infra.repositories.TipoVisitaRepository;

public class TipoVisitaRepositoryJpa implements TipoVisitaRepository {

private final EntityManager em;
	
	public TipoVisitaRepositoryJpa(EntityManager em) {
		this.em = em;
	}
	
	@Override
	public List<TipoVisita> findAll() {
		
		List<TipoVisita> tipoVisitas = new ArrayList<TipoVisita>();
		
		try {
			List<TipoVisita> resultSet = em.createQuery("FROM TipoVisita").getResultList();
			
			tipoVisitas.addAll(resultSet);
			
			em.close();
//			PersistenceManager.close();
		} catch (Exception e) {
			e.printStackTrace();
			em.close();
			PersistenceManager.close();
		}
		
		return tipoVisitas;
	}

}
