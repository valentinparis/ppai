package ar.utn.ppai.disenio.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "escuela")
public class Escuela implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idescuela")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	
	@Column(name = "nombre")
	private String nombre;

	public Escuela() {}

	public Escuela(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Escuela [id=" + id + ", nombre=" + nombre + "]";
	}
	
	
}
