package ar.utn.ppai.disenio.modelo;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "exposicion")
public class Exposicion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idexposicion")
	private int id;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "fecha_hora_inicio")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaHoraInicio;
	
	@Column(name = "fecha_hora_fin")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaHoraFin;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "exposiciones_x_publico", joinColumns = @JoinColumn(name = "idexposicion"),
    	inverseJoinColumns = @JoinColumn(name = "idpublicodestino"),
    	uniqueConstraints = {@UniqueConstraint(columnNames = {"idexposicion", "idpublicodestino"})})
	private List<PublicoDestino> publicos;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinColumn(name = "id_detalle_exposicion")
	private List<DetalleExposicion> detalleExposicion;

	@Column(name = "hora_apertura")
	private LocalTime horaApertura;
	
	@Column(name = "hora_cierre")
	private LocalTime horaCierre;


	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idtipoexposicion")
	private TipoExposicion tipoExposicion;

	public Exposicion() {
		this("", null);
	}
	
	public Exposicion(String nombre, TipoExposicion tipoExposicion) {
		
		this.nombre = nombre;
		this.tipoExposicion = tipoExposicion;
		this.publicos = new ArrayList<>();
		this.detalleExposicion = new ArrayList<>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaHoraInicio() {
		return fechaHoraInicio;
	}

	public void setFechaHoraInicio(Date fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

	public Date getFechaHoraFin() {
		return fechaHoraFin;
	}

	public void setFechaHoraFin(Date fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}

	public TipoExposicion getTipoExposicion() {
		return tipoExposicion;
	}

	public List<PublicoDestino> getPublicos() {
		return publicos;
	}

	public void setPublicos(List<PublicoDestino> publicos) {
		this.publicos = publicos;
	}

	public LocalTime getHoraApertura() {
		return horaApertura;
	}

	public void setHoraApertura(LocalTime horaApertura) {
		this.horaApertura = horaApertura;
	}

	public LocalTime getHoraCierre() {
		return horaCierre;
	}

	public void setHoraCierre(LocalTime horaCierre) {
		this.horaCierre = horaCierre;
	}

	public void setTipoExposicion(TipoExposicion tipoExposicion) {
		this.tipoExposicion = tipoExposicion;
	}

	public List<DetalleExposicion> getDetalleExposicion() {
		return detalleExposicion;
	}

	public void setDetalleExposicion(List<DetalleExposicion> detalleExposicion) {
		this.detalleExposicion = detalleExposicion;
	}

	public int calcDuracionEstExpo() {
		System.out.println("------------------- INICIO calcDuracionEstExpo --------------------");
		System.out.println("detalleExposicion: " +  detalleExposicion.size());
		
		int duracionEst = 0;
		if(detalleExposicion.size() > 0 && detalleExposicion != null) {
			for (DetalleExposicion detalleExposicion : detalleExposicion) {
				System.out.println("detalleExposicion.buscarDuracionExtObra(): " + detalleExposicion.buscarDuracionExtObra());
				duracionEst += detalleExposicion.buscarDuracionExtObra();
			}
		}
		
		System.out.println("------------------- FIN calcDuracionEstExpo --------------------");
		return duracionEst;
	}
	
	public int calcDuracionResumidaExpo() {
		int duracionRes = 0;
		if(detalleExposicion.size() > 0 && detalleExposicion != null) {
			for (DetalleExposicion detalleExposicion : detalleExposicion) {
				System.out.println("detalleExposicion.buscarDuracionExtObra(): " + detalleExposicion.buscarDuracionResumidaObra());
				duracionRes += detalleExposicion.buscarDuracionResumidaObra();
			}
		}
		
		System.out.println("------------------- FIN calcDuracionResumidaExpo --------------------");
		return duracionRes;
	}

	public boolean esVigente(Date actual) {
		return actual.before(fechaHoraFin);
	}

	public String getHorarioHabilitado() {
		return "Hora Apertura: " + horaApertura + ", Hora Cierre: " + horaCierre;
	}
	
	public boolean getTempVigente(Date actual, String tipoExposicion) {
		boolean esVigenteYActual = this.esVigente(actual) && this.tipoExposicion.esTemporal(tipoExposicion);
		
		if (esVigenteYActual) {
			return true;
		}
		
		return false;
	}

	@Override
	public String toString() {
		return "Exposicion [id=" + id + ", nombre=" + nombre + ", fechaHoraInicio=" + fechaHoraInicio
				+ ", fechaHoraFin=" + fechaHoraFin + ", publicos=" + publicos + ", Horario Habilitado= "+ getHorarioHabilitado() +
				", tipoExposicion=" + tipoExposicion + "]";
	}
	
}
