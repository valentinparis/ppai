package ar.utn.ppai.disenio.modelo.infra.repositories;

import java.util.List;

import ar.utn.ppai.disenio.modelo.Exposicion;

public interface ExposicionRepository {
	List<Exposicion> findAll();
}
