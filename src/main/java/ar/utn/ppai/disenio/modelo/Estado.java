package ar.utn.ppai.disenio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "estado")
public class Estado {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idestado")
	private int id;
	
	@Column(name = "ambito")
	private String ambito;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "descripcion")
	private String descripcion;

	public Estado() {
	}

	public Estado(String ambito, String nombre, String descripcion) {
		this.ambito = ambito;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public String getAmbito() {
		return ambito;
	}

	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		return "Estado [ambito=" + ambito + ", nombre=" + nombre + ", descripcion=" + descripcion + "]";
	}

	public void esAmbitoReserva() {}
	
	public void esPendienteConfirmacion() {}

}
