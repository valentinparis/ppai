package ar.utn.ppai.disenio.modelo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "asignacionvisita")
public class AsignacionVisita {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idasignacionvisita")
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fechaHoraInicio")
	private Date fechaHoraInicio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fechaHoraFin")
	private Date fechaHoraFin;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idempleado")
	private Empleado empleado;
	
	@ManyToOne
	@JoinColumn(name = "idreservavisita")
	private ReservaVisita reservaVisita;
	
	public AsignacionVisita() {}
	
	public AsignacionVisita(Date fechaHoraInicio, Date fechaHoraFin, ReservaVisita reservaVisita) {
		this.fechaHoraInicio = fechaHoraInicio;
		this.fechaHoraFin = fechaHoraFin;
		this.reservaVisita = reservaVisita;
	}
	
	public Date getFechaHoraInicio() {
		return fechaHoraInicio;
	}
	public void setFechaHoraInicio(Date fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}
	public Date getFechaHoraFin() {
		return fechaHoraFin;
	}
	public void setFechaHoraFin(Date fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public ReservaVisita getReservaVisita() {
		return reservaVisita;
	}

	public void setReservaVisita(ReservaVisita reservaVisita) {
		this.reservaVisita = reservaVisita;
	}

	@Override
	public String toString() {
		return "AsignacionVisita [id=" + id + ", fechaHoraInicio=" + fechaHoraInicio.getTime() + ", fechaHoraFin=" + fechaHoraFin.getTime()
				+ ", empleado=" + empleado + ", reservaVisita=" + reservaVisita + "]";
	}

	public boolean esAsignacionParaFechaHora() {
		return true;
	}
}
