package ar.utn.ppai.disenio.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "horarioempleado")
public class HorarioEmpleado {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idhorarioempleado")
	private int id;
	
	@Column(name = "fechaHoraIngreso")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaHoraIngreso;
	
	@Column(name = "fechaHoraSalida")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaHoraSalida;
	
	public HorarioEmpleado() {}
	
	public HorarioEmpleado(Date fechaHoraIngreso, Date fechaHoraSalida) {
		this.fechaHoraIngreso = fechaHoraIngreso;
		this.fechaHoraSalida = fechaHoraSalida;
	}

	public Date getFechaHoraIngreso() {
		return fechaHoraIngreso;
	}
	public void setFechaHoraIngreso(Date fechaHoraIngreso) {
		this.fechaHoraIngreso = fechaHoraIngreso;
	}
	public Date getFechaHoraSalida() {
		return fechaHoraSalida;
	}
	public void setFechaHoraSalida(Date fechaHoraSalida) {
		this.fechaHoraSalida = fechaHoraSalida;
	}

	public void dispEnFechaHoraReserva() {}
	
}
