package ar.utn.ppai.disenio.modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sede")
public class Sede {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idsede")
	private int id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "cantMaxVisit")
	private int cantidadMaximaVisitante;

	@Column(name = "cantMaxGuia")
	private int cantMaxPorGuia;
	
	@OneToMany(mappedBy = "sede")
	private List<ReservaVisita> reservaVisita;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_sede")
	private List<Exposicion> exposiciones;

	public Sede(String nombre, int cantidadMaximaVisitante, int cantMaxPorGuia) {
		this.nombre = nombre;
		this.cantidadMaximaVisitante = cantidadMaximaVisitante;
		this.cantMaxPorGuia = cantMaxPorGuia;
		this.exposiciones = new ArrayList<Exposicion>();
		this.reservaVisita = new ArrayList<ReservaVisita>();
	}

	public Sede() {
		this("", 0, 0);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidadMaximaVisitante() {
		return cantidadMaximaVisitante;
	}

	public void setCantidadMaximaVisitante(int cantidadMaximaVisitante) {
		this.cantidadMaximaVisitante = cantidadMaximaVisitante;
	}

	public int getCantMaxPorGuia() {
		return cantMaxPorGuia;
	}

	public void setCantMaxPorGuia(int cantMaxPorGuia) {
		this.cantMaxPorGuia = cantMaxPorGuia;
	}
	
	public List<ReservaVisita> getReservaVisita() {
		return reservaVisita;
	}

	public void setReservaVisita(List<ReservaVisita> reservaVisita) {
		this.reservaVisita = reservaVisita;
	}

	public void buscarReservaParaHoraYFecha(Sede sede) {
		
		List<ReservaVisita> reservasDisp = new ArrayList<ReservaVisita>();
		
		if(this.reservaVisita.size() < 0 && this.reservaVisita != null) {
			for (ReservaVisita reservaVisita : reservaVisita) {
				reservaVisita.getFechaHoraReserva();
			}
		}
	}

	public List<Exposicion> exposicionSedeSeleccionada(Date actual, String tipoExpo) {
		List<Exposicion> expoTempYVig = new ArrayList<Exposicion>();
		
		for (Exposicion exposicion : exposiciones) {
			if (!exposicion.getTempVigente(actual, tipoExpo)) {
				expoTempYVig.add(exposicion);
			}
		}
		
		return expoTempYVig;
	}

	public int obtenerDuracionReserva(List<Exposicion> exposicionesSelecc, String tipoVisita) {
		System.out.println("------------------- INICIO obtenerDuracionReserva --------------------");
		
		int duracion = 0;
		if(exposicionesSelecc.size() <= 0 || exposicionesSelecc == null) {
			return -1;
		}
			
		for (Exposicion exposicion : exposicionesSelecc) {
			if(tipoVisita.equalsIgnoreCase("Por Exposición")) {
				duracion += exposicion.calcDuracionEstExpo();
			} else if (tipoVisita.equalsIgnoreCase("Completa")) {
				duracion += exposicion.calcDuracionResumidaExpo();
			}
		}
		
		System.out.println("------------------- FIN obtenerDuracionReserva --------------------");
		return duracion;
	}

	public void verificarCantidadMaximaDeVisitantes() {
	}

	public List<Exposicion> getExposiciones() {
		return exposiciones;
	}

	public void setExposiciones(List<Exposicion> exposiciones) {
		this.exposiciones = exposiciones;
	}
	
	public void agregarExposicion(Exposicion expo) {
		this.exposiciones.add(expo);
	}

	@Override
	public String toString() {
		return "Sede [nombre=" + nombre + ", cantidadMaximaVisitante=" + cantidadMaximaVisitante + ", cantMaxPorGuia="
				+ cantMaxPorGuia + ", reservaVisita=" + reservaVisita + "]";
	}
	
}
