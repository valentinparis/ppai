package ar.utn.ppai.disenio.modelo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sesion")
public class Sesion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idsesion")
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fechaHoraFin")
	private Date fechaHoraFin;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fechaHoraInicio")
	private Date fechaHoraInicio;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idusuario")
	private Usuario usuario;
	
	public Sesion() {}
	
	public Sesion(Date fechaHoraFin, Date fechaHoraInicio, Usuario usuario) {
		this.fechaHoraFin = fechaHoraFin;
		this.fechaHoraInicio = fechaHoraInicio;
		this.usuario = usuario;
	}

	public Date getFechaHoraFin() {
		return fechaHoraFin;
	}
	public void setFechaHoraFin(Date fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}
	public Date getFechaHoraInicio() {
		return fechaHoraInicio;
	}
	public void setFechaHoraInicio(Date fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@Override
	public String toString() {
		return "Sesion [fechaHoraFin=" + fechaHoraFin.getTime() + ", fechaHoraInicio=" + fechaHoraInicio.getTime() + ", usuario=" + usuario
				+ "]";
	}

	public void getEmpleadoEnSesion() {}
	
}
