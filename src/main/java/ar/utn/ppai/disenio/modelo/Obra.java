package ar.utn.ppai.disenio.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "obra")
public class Obra {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idobra")
	private int id;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "duracionExtendida")
	private int duracionExtendida;
	
	@Column(name = "duracionResumida")
	private int duracionResumida;
	
	public Obra() {}
	
	public Obra(String nombre,int duracionExtendida, int duracionResumida) {
		this.nombre = nombre;
		this.duracionExtendida = duracionExtendida;
		this.duracionResumida = duracionResumida;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getDuracionExtendida() {
		return duracionExtendida;
	}
	public void setDuracionExtendida(int duracionExtendida) {
		this.duracionExtendida = duracionExtendida;
	}

	public int getDuracionResumida() {
		return duracionResumida;
	}

	public void setDuracionResumida(int duracionResumida) {
		this.duracionResumida = duracionResumida;
	}

	@Override
	public String toString() {
		return "Obra [id=" + id + ", nombre=" + nombre + ", duracionExtendida=" + duracionExtendida
				+ ", duracionResumida=" + duracionResumida + "]";
	}
	
	
}
