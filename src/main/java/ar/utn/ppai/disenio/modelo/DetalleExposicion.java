package ar.utn.ppai.disenio.modelo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "detalleexposicion")
public class DetalleExposicion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "iddetalleexposicion")
	private int id;
	
	@Column(name = "lugarAsignado")
	private String lugarAsignado;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "idobra")
	private Obra obra;
	
	public DetalleExposicion() {}	
	
	public DetalleExposicion(String lugarAsignado, Obra obra) {
		this.lugarAsignado = lugarAsignado;
		this.obra = obra;
	}

	public String getLugarAsignado() {
		return lugarAsignado;
	}
	public void setLugarAsignado(String lugarAsignado) {
		this.lugarAsignado = lugarAsignado;
	}

	public Obra getObra() {
		return obra;
	}

	public void setObra(Obra obra) {
		this.obra = obra;
	}

	public int buscarDuracionExtObra() {
		System.out.println("------------------- INICIO calcDuracionEstExpo --------------------");
		System.out.println("obra.getDuracionExtendida(): " + obra.getDuracionExtendida());
		System.out.println("------------------- INICIO calcDuracionEstExpo --------------------");
		return obra.getDuracionExtendida();
	}
	
	public int buscarDuracionResumidaObra() {
		System.out.println("------------------- INICIO buscarDuracionResumidaObra --------------------");
		System.out.println("obra.getDuracionResumida(): " + obra.getDuracionResumida());
		System.out.println("------------------- INICIO buscarDuracionResumidaObra --------------------");
		return obra.getDuracionResumida();
	}
	
}
