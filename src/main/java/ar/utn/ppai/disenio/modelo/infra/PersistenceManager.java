package ar.utn.ppai.disenio.modelo.infra;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceManager {
	
	private static EntityManagerFactory emf;
	private static PersistenceManager persistenceManager;
	
	private PersistenceManager() {
		emf = Persistence.createEntityManagerFactory("Persistencia");
	}
	
	public static PersistenceManager getInstance() {
		if (persistenceManager == null) persistenceManager = new PersistenceManager();
		return persistenceManager;
	}
	
	public EntityManager entityManager() {
		return emf.createEntityManager();
	}
	
	public static void close() {
		emf.close();
	}

}
