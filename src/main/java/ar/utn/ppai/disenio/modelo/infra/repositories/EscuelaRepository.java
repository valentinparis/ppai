package ar.utn.ppai.disenio.modelo.infra.repositories;

import java.util.List;

import ar.utn.ppai.disenio.modelo.Escuela;

public interface EscuelaRepository {
	List<Escuela> findAll();
}
